#!/bin/bash
# Initial bootstrapping.
# File inception by Huki (2015-05-01).

# Init assets
if [ -e "assets/.git" ]; then
  mkdir -p assets/cache
  mkdir -p assets/profiles
  mkdir -p assets/replays
  mkdir -p assets/times
fi

# Init bin
mkdir -p bin/linux
mkdir -p bin/win32
mkdir -p bin/win64
mkdir -p bin/macos

android=(
  "armeabi-v7a"
  "arm64-v8a"
  "x86"
  "x86_64"
)

for name in ${android[@]}; do
  mkdir -p bin/android/$name
done
